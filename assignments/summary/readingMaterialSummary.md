<h3>What is IOT?</h3>
IOT stands for internet of things. IOT Devices are connected to internet and shares the data to the server which can be remotely monitored and configured. These devices can be controlled from remote location. 

<br><h3>INDUSTRIAL REVOLUTION</h3></br>
![IOT Revolution](/extras/IOT Revolution.png)

<h3>What is Industrial IOT?</h3>

Industrial IOT include Sensors,Automation tools,Cloud servers and applications.

<h3>How Industrial IOT works?</h3>
<br>1. We have sensors installed at points in factories. They send data to Iot gateways.</br> 
<br>2. Iot gateways transmit and receive data to cloud.</br>
<br>3. Application programs are designed to handle all data.</br> 
<br>4. We can access data through mobile applications.</br>

<h3>What is Industry 3.0?</h3>

![industry 3.0](/extras/industrial 3.0.png)

>In industry 3.0 data is stored in databases and exported in excel.

1. field devices - sensors , actuators
2. control level - PLC ,PC's ,PID used to control the machinery
3. supervisory level - SCADA(supervisory control and data acquistion) software to control operations.
4. planning level - management excutive system
5. management level - ERP

<h3>What are the protocols in industry 3.0 and why we use?</h3>

#Protocols are -
1. Modbus 
2. CAN open
3. Ether cat

> These protocols are used by sensors to send data to PLC's. These are also called as field bus.
> These protocols are optimized to send data to central server inside the factory.

<h3>What is Industry 4.0?</h3>

Industry 4.0 are same devices we use in industry 3.0, basic difference is they are connected to internet and share data to cloud. Getting data from controllers and converting into protocols in cloud.

![industry 4.0](/extras/industry 3.0.jpg)


<h3>How industry 4.0 works(ARCHITECTURE)</h3>

1. Sensor,actuators and rtu send data to controllers(PLC,DCS,CNC). This data gather in SCADA and ERP.  Controllers send data to opc server via control bus. SCADA historian send data via ethernet to opc server. ERP/MES send data to cloud via REST-API.
2. OPC server send data to IOT gateway via OPC-UA or DA.
3. IOT gateway send to IOT hub through protocols such as websocket MQTT.

<h3>What are protocols of industry4.0 and why we use?</h3>
<br>1. MQtt</br>
<br>2. Restfullapi</br>
<br>3. Websockets</br>
<br>4. http</br>
<br>5. OPC UA.</br>
 
 > All these protocols are designed to send data to cloud for data Analytics. 

<h3>Can we upgrade from industry3.0 to industry 4.0?</h3>

Yes, we can upgrade from indsutry3.0 devices to industry 4.0
#Solution - 
> We have to take data from industry 3.0 devices and then send data to cloud using industry 4.0 devices.

<h3>Can we change industry3.0 protocols to industry4.0 protocols?</h3>
Yes, we convert industry3.0 protocols to industry 4.0 but there challenges such as - 
<br>1. Expensive hardware. </br>
<br>2. Lack of documentation </br>
<br>3. Properitary PLC protocol.</br>

>We have library that get data from industry3.0 and convert into industry4.0

<h3>What are the tools to analyze data we have received to cloud?</h3>
<br>1. IOT tsdb tool  - store your data in time series databases</br>
<br>2. IOT dashboards - view data on dashboards.</br>
<br>3. IOT platforms - Analyze data on platforms such as AWS,AZURE,GOOGLE,THINGSBOARD.</br>
<br>4. Get alerts - get alerts based on data in platforms such as zaiper,twilio.</br>




